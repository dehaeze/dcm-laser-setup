Ts = 1e-4; % Sampling Time [s]

s = tf('s');

w0_int = 2*pi/10;
G_int_lpf = 1/(1 + 2*0.7*s/w0_int + s^2/w0_int^2);
G_int_lpf = c2d(G_int_lpf, Ts, 'tustin');

w0_int = 2*pi*0.5;
G_gain_lpf = 1/(1 + 2*0.7*s/w0_int + s^2/w0_int^2);
G_gain_lpf = c2d(G_gain_lpf, Ts, 'tustin');

w0_int = 2*pi*0.5;
G_gain_hpf = s^2/w0_int^2/(1 + 2*0.7*s/w0_int + s^2/w0_int^2);
G_gain_hpf = c2d(G_gain_hpf, Ts, 'tustin');

clear w0_int s