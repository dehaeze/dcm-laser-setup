%% Get Data from Speedgoat
tg = slrt;

f = SimulinkRealTime.openFTP(tg);
mget(f, 'data_cal.dat');
close(f);

%% Load the data on the Workspace
data_dat = SimulinkRealTime.utils.getFileScopeData('data_cal.dat');

data = struct();
data.Dx = data_dat.data(:,1); % [-]
data.Dy = data_dat.data(:,2); % [-]
data.Rx = data_dat.data(:,3); % [-]
data.Ry = data_dat.data(:,4); % [-]
data.t = data_dat.data(:,end) - data_dat.data(1,end); % [s]

data.Ts = (data.t(end) - data.t(1))/(length(data.t)-1);
data.Fs = 1/data.Ts;

%%
save('mat/meas_noise_metrology_high_freq.mat', '-struct', 'data')


%% Load Data
clear all;
load('meas_noise_metrology_high_freq.mat');

Gr = 100e3; % [nrad/-]
Gd = 1000e3; % [nm/-]

%% Angular Noise
figure;
hold on;
plot(t, detrend(Gr*Ry, 0), 'DisplayName', 'Ry')
plot(t, detrend(Gr*Rx, 0), 'DisplayName', 'Rx')
hold off;
legend();
xlabel('Time [s]');
ylabel('Angle [nrad]');

%% Position Noise
figure;
hold on;
plot(t, detrend(Gd*Dy, 0), 'DisplayName', 'Dy')
plot(t, detrend(Gd*Dx, 0), 'DisplayName', 'Dx')
hold off;
legend();
xlabel('Time [s]');
ylabel('Position [nm]');

%% Power Spectral Density
win = hanning(ceil(10*Fs)); % Hannning Windows

[Pdx, f] = pwelch(detrend(Gd*Dx, 0), win, [], [], 1/Ts);
[Pdy, ~] = pwelch(detrend(Gd*Dy, 0), win, [], [], 1/Ts);
[Prx, ~] = pwelch(detrend(Gr*Rx, 0), win, [], [], 1/Ts);
[Pry, ~] = pwelch(detrend(Gr*Ry, 0), win, [], [], 1/Ts);

%% Plot the ASD of displacements
figure;
hold on;
plot(f, sqrt(Pdx), 'DisplayName', 'Dx')
plot(f, sqrt(Pdy), 'DisplayName', 'Dy')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [$nm/\sqrt{Hz}$]');
xlim([0.1, 5e3]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 1);

%% Plot the ASD of rotations
figure;
hold on;
plot(f, sqrt(Prx), 'DisplayName', 'Rx')
plot(f, sqrt(Pry), 'DisplayName', 'Ry')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [$nrad/\sqrt{Hz}$]');
xlim([0.1, 5e3]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 1);

%% Cumulative Amplitude Spectrum
CPS_Dx = cumtrapz(f, Pdx);
CPS_Dy = cumtrapz(f, Pdy);
CPS_Rx = cumtrapz(f, Prx);
CPS_Ry = cumtrapz(f, Pry);

%% CPS Translations
figure;
hold on;
plot(f, sqrt(CPS_Dx), 'DisplayName', 'Dx');
plot(f, sqrt(CPS_Dy), 'DisplayName', 'Dy');
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Cumulative ASD [nm RMS]');
xlim([0.1, 5e3]);

%% CPS Rotations
figure;
hold on;
plot(f, sqrt(CPS_Rx), 'DisplayName', 'Rx');
plot(f, sqrt(CPS_Ry), 'DisplayName', 'Ry');
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Cumulative ASD [nrad RMS]');
xlim([0.1, 5e3]);

%% 2DoF Cumulative Amplitude Spectrum
T_int = logspace(-1, 1, 100);
RMS_2dof = zeros(length(T_int), length(f));

for i = 1:length(T_int)
    i_filt = f>1/T_int(i);
    RMS_2dof(i, find(i_filt, 1, 'first'):find(i_filt, 1, 'last')) = cumtrapz(f(i_filt), Prx(i_filt));
end

%%
figure;
[~, c] = contour(T_int, f, RMS_2dof', [2, 5, 10, 20, 50, 100, 200, 500], 'ShowText','on');
c.LineWidth = 3;
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
xlabel('Time of interest [s]');
ylabel('Bandwidth [Hz]');
