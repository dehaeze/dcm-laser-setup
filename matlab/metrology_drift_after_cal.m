%% Get Data from Speedgoat
tg = slrt;

f = SimulinkRealTime.openFTP(tg);
mget(f, 'data.dat');
close(f);

%% Load the data on the Workspace
data_dat = SimulinkRealTime.utils.getFileScopeData('data.dat');

data = struct();
data.Rx = data_dat.data(:,1)*1e3; % [nrad]
data.Ry = data_dat.data(:,2); % [nrad]
data.Rz = data_dat.data(:,3)/(0.47^7); % [nrad]
data.t = data_dat.data(:,end);

data.Ts = (data.t(end) - data.t(1))/(length(data.t)-1);
data.Fs = 1/data.Ts;

save('mat/first_comp_4qd_int.mat', '-struct', 'data')

%%
clear all;
load('first_comp_4qd_int.mat');

%%
i = t > 3600*10 & t < 3600*15;

figure;
plot(t(i)/3600, detrend(Rx(i), 0));
xlabel('Time [h]');
ylabel('Rx [nrad]');

%%
figure;
hold on;
plot(t(i)/3600, detrend(Rx(i), 0));
plot(t(i)/3600, detrend(Rz(i), 0));
xlabel('Time [h]');
ylabel('Rx [nrad]');
