%% Get Data from Speedgoat
tg = slrt;

f = SimulinkRealTime.openFTP(tg);
mget(f, 'data.dat');
close(f);

%% Load the data on the Workspace
data_dat = SimulinkRealTime.utils.getFileScopeData('data.dat');

data = struct();
data.Rx = data_dat.data(:,1);
data.Ry = data_dat.data(:,2);
data.Rz = data_dat.data(:,3);
data.t = data_dat.data(:,end);

data.Ts = (data.t(end) - data.t(1))/(length(data.t)-1);
data.Fs = 1/data.Ts;

%%
i_filt = data.t > 400;

figure;
hold on;
plot(data.t(i_filt),     detrend(data.Rz(i_filt), 0));
plot(data.t(i_filt), 3e3*detrend(data.Rx(i_filt), 0));
plot(data.t(i_filt), 6e2*detrend(data.Ry(i_filt), 0));
hold off;

%%
win = hanning(ceil(10*data.Fs)); % Hannning Windows

[PRz, f] = pwelch(detrend(data.Rz, 0), win, [], [], 1/data.Ts);
[PRy, ~] = pwelch(detrend(data.Ry, 0), win, [], [], 1/data.Ts);
[PRx, ~] = pwelch(detrend(data.Rx, 0), win, [], [], 1/data.Ts);

%% Plot ASD of ADC
figure;
hold on;
plot(f, sqrt(PRz), 'DisplayName', 'Rz')
plot(f, 5e3*sqrt(PRx), 'DisplayName', 'Rx')
plot(f, 5e3*sqrt(PRy), 'DisplayName', 'Ry')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [$nrad/\sqrt{Hz}$]');
xlim([0.2, 5e3]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 2);

%%
CPS_Rz = flip(-cumtrapz(flip(f), flip(PRz)));
CPS_Ry = flip(-cumtrapz(flip(f), flip(PRy)));
CPS_Rx = flip(-cumtrapz(flip(f), flip(PRx)));

figure;
hold on;
plot(f, sqrt(CPS_Rz), 'DisplayName', 'Rz')
plot(f, 5e3*sqrt(CPS_Rx), 'DisplayName', 'Rx')
plot(f, 5e3*sqrt(CPS_Ry), 'DisplayName', 'Ry')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Cumulative Ampl. Spectr. [nrad]');
xlim([0.2, 5e3]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 2);


%% Save Data in Mat file
save('mat/test-night-protection.mat', '-struct', 'data')
%%
load('mat/test-night.mat');
a = load('mat/test-night-protection.mat');
figure;
hold on;
plot(t/3600, detrend(x2, 0));
plot(a.t/3600, detrend(a.x2, 0));
hold off;
xlabel('Time [h]')
