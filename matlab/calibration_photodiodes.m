%% Clear Workspace and Close figures
clear; close all; clc;

%% Intialize Laplace variable
s = zpk('s');

%% Path for functions, data and scripts
addpath('./mat/'); % Path for data
addpath('./src/'); % Path for functions

%% Colors for the figures
colors = colororder;

%% Frequency Vector
freqs = logspace(1, 3, 1000);

aa=[[-60,-4.77,-1.25,1.45]; [-30,-3.74,-0.29,2.46]; [0,-2.75,0.75,3.48]; [30,-1.74,1.78,4.49]; [60,-0.73,2.73,5.45]];
aa

%% Load Measurement Data
Dx = load('calib_4qd_Dx.mat'); % X scan
Dy = load('calib_4qd_Dy.mat'); % Y Scan

%% Capacitive Sensor Gain
G_capa = 30; % [um/V]

%% Position of the beam as measured by the quadrant photodiode during the two scans
figure;
hold on;
plot(Dx.Dx, Dx.Dy, 'DisplayName', 'Dx scan')
plot(Dy.Dx, Dy.Dy, 'DisplayName', 'Dy scan')
hold off;
axis equal
xlabel('4QD Dx [-]'); ylabel('4QD Dy [-]');
xlim([-0.1, 0.1]); ylim([-0.1, 0.1]);
legend();

%% Remove offset of capacitive sensor
% so that 0 of capacitive equal center of the 4QD
[~, i_Dx] = min(abs(Dx.Dx));
[~, i_Dy] = min(abs(Dy.Dy));

Dx.Px = Dx.Px - Dx.Px(i_Dx);
Dy.Py = Dy.Py - Dy.Py(i_Dy);

%% Quadrant Photodiode measurement as a function of its position
figure;
hold on;
plot(G_capa*Dx.Px, Dx.Dx, 'DisplayName', 'Dx scan')
plot(G_capa*Dy.Py, Dy.Dy, 'DisplayName', 'Dy scan')
hold off;
xlabel('Piezo Displacement [$\mu$m]');
ylabel('4QD measurement [-]');
xlim([-100, 100]); ylim([-0.1, 0.1]);
legend();

%% Linear fit [um/-]
G_Dx = (Dx.Dx)\(G_capa*Dx.Px);
G_Dy = (Dy.Dy)\(G_capa*Dy.Py);

%% Quadrant Photodiode measurement as a function of its position - Linear and Sigmoid fit
figure;
hold on;
plot(30*Dx.Px, Dx.Dx, 'DisplayName', 'Dx scan')
plot(30*Dy.Py, Dy.Dy, 'DisplayName', 'Dy scan')
plot([-100, 100], 1/G_Dx*[-100, 100], 'k--', 'DisplayName', 'Dx Fit')
plot([-100, 100], 1/G_Dy*[-100, 100], 'k-.', 'DisplayName', 'Dy Fit')
hold off;
xlabel('Piezo Displacement [$\mu$m]');
ylabel('4QD measurement [-]');
xlim([-100, 100]); ylim([-0.1, 0.1]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 2);

%% Load Measurement Data
Rx = load('calib_4qd_Rx.mat');
Ry = load('calib_4qd_Ry.mat');

%% Capacitive Sensor Gain
G_capa = 30; % [um/V]

%% Position of the beam as measured by the quadrant photodiode during the two scans
figure;
hold on;
plot(Rx.Rx, Rx.Ry, 'DisplayName', 'Rx scan')
plot(Ry.Rx, Ry.Ry, 'DisplayName', 'Ry scan')
hold off;
axis equal
xlabel('4QD Rx [-]'); ylabel('4QD Ry [-]');
xlim([-1, 1]); ylim([-1, 1]);
legend();

%% Remove offset of capacitive sensor
% so that 0 of capacitive equal center of the 4QD
[~, i_Rx] = min(abs(Rx.Rx));
[~, i_Ry] = min(abs(Ry.Ry));

Rx.Px = Rx.Px - Rx.Px(i_Rx);
Ry.Py = Ry.Py - Ry.Py(i_Ry);

%% Quadrant Photodiode measurement as a function of its position
figure;
hold on;
plot(G_capa*Rx.Px, Rx.Rx, 'DisplayName', 'Rx scan')
plot(G_capa*Ry.Py, Ry.Ry, 'DisplayName', 'Ry scan')
hold off;
xlabel('Piezo Displacement [$\mu$m]');
ylabel('4QD measurement [-]');
xlim([-150, 150]); ylim([-1, 1]);
legend();

%% Estimation of linear region
i_Rx = Rx.Rx < 0.1 & Rx.Rx > -0.1;
i_Ry = Ry.Ry < 0.1 & Ry.Ry > -0.1;

%% Linear fit near origin
G_Rx = (Rx.Rx(i_Rx))\(G_capa*Rx.Px(i_Rx)); % Gain in [um/-]
G_Ry = (Ry.Ry(i_Ry))\(G_capa*Ry.Py(i_Ry)); % Gain in [um/-]

%% Sigmoid Fit
A1 = -1;
A2 =  1;
k = -19.5;
xa = 0;

x = -150:1:150; % Linear position [um]
y = (A1-A2)./(1 + exp((x-xa)/k)) + A2; % Sigmoid Function [-]

%% Quadrant Photodiode measurement as a function of its position - Linear and Sigmoid fit
figure;
hold on;
plot(30*Rx.Px, Rx.Rx, 'DisplayName', 'Rx scan')
plot(30*Ry.Py, Ry.Ry, 'DisplayName', 'Ry scan')
plot(x, y, 'k--', 'DisplayName', 'Sigmoid fit')
plot([-100, 100], -1/40*[-100, 100], '--', 'DisplayName', 'Linear Fit')
hold off;
xlabel('Piezo Displacement [$\mu$m]');
ylabel('4QD measurement [-]');
xlim([-150, 150]); ylim([-1, 1]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 2);

%% Beam Radius (1/e2) estimation
[~, i_Rx_high] = min(abs(Rx.Rx - 0.95));
[~, i_Rx_low]  = min(abs(Rx.Rx + 0.95));
Rx_radius = 30*(Rx.Px(i_Rx_low) - Rx.Px(i_Rx_high))/2;

[~, i_Ry_high] = min(abs(Ry.Ry - 0.95));
[~, i_Ry_low]  = min(abs(Ry.Ry + 0.95));
Ry_radius = 30*(Ry.Py(i_Ry_low) - Ry.Py(i_Ry_high))/2;

f = 0.4; % Focal of lens [m]

%% Beam Angle as a function of the measured quadrant photodiode normalized output
figure;
hold on;
plot(Rx.Rx, 1/f * G_capa*Rx.Px)
plot([-1, 1], 1/f * G_Rx * [-1, 1], 'k--')
hold off;
xlabel('4QD normalized output [-]');
ylabel('Beam Angle [$\mu$rad]');
xlim([-1, 1]);
ylim([-150, 150]);
