%% Get Data from Speedgoat
tg = slrt;

f = SimulinkRealTime.openFTP(tg);
mget(f, 'data.dat');
close(f);

%% Load the data on the Workspace
data_dat = SimulinkRealTime.utils.getFileScopeData('data.dat');

data = struct();
data.Dul = data_dat.data(:,1);
data.Ddl = data_dat.data(:,2);
data.Ddr = data_dat.data(:,3);
data.Dur = data_dat.data(:,4);
data.Rul = data_dat.data(:,5);
data.Rdl = data_dat.data(:,6);
data.Rdr = data_dat.data(:,7);
data.Rur = data_dat.data(:,8);
data.t = data_dat.data(:,end);

data.Ts = (data.t(end) - data.t(1))/(length(data.t)-1);
data.Fs = 1/data.Ts;

save('mat/meas_4qd_ampl_noise.mat', '-struct', 'data')

%% Measurement of the noise without light
% The 4QD is connected to the transimpedance amplifier and to the ADC
% through the 5m coaxial cable.
clear; clc; close all;
colors = colororder;

%% Load measured voltages on the 4 quadrants
load('mat/meas_4qd_noise_no_light.mat')

%% Remove first points
ur = ur(5:end);
ul = ul(5:end);
dr = dr(5:end);
dl = dl(5:end);
t = t(5:end);

%% PSD of measured voltage is computed
win = hanning(ceil(1*Fs)); % Hannning Windows

[Pur, f] = pwelch(detrend(ur, 0), win, [], [], 1/Ts);
[Pul, ~] = pwelch(detrend(ul, 0), win, [], [], 1/Ts);
[Pdr, ~] = pwelch(detrend(dr, 0), win, [], [], 1/Ts);
[Pdl, ~] = pwelch(detrend(dl, 0), win, [], [], 1/Ts);

%% Plot the ASD of the measured voltages without any light
figure;
hold on;
plot(f, sqrt(Pur), 'DisplayName', 'ur')
plot(f, sqrt(Pul), 'DisplayName', 'ul')
plot(f, sqrt(Pdr), 'DisplayName', 'dr')
plot(f, sqrt(Pdl), 'DisplayName', 'dl')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [$V/\sqrt{Hz}$]');
xlim([1, 5e3]); ylim([1e-6, 1e-2]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 1);

exportFig('figs/4qd_noise_no_light', 'height', 'normal', 'width', 'large')

%% Noise estimation in rotation
V_mean = 1.5; % [v]

G = 100e3; % Gain in [nrad/-]

Rx = G*(ur + ul - dr - dl)./(4*V_mean + ur + ul + dr + dl); % [nrad]

figure;
plot(t, Rx)
xlabel('Time [s]');
ylabel('Rotation [nrad]');

[Pxx, f] = pwelch(Rx, win, [], [], 1/Ts);

figure;
plot(f, sqrt(Pxx))
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [nrad/$\sqrt{Hz}$]');
xlim([1, 5e3]);

CPS_xx = cumtrapz(f, Pxx);

figure;
plot(f, sqrt(CPS_xx))
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Cumulative ASD [nrad]');
xlim([1, 5e3]);

sprintf('Electronic Noise for Rotations = %.1f [nrad RMS] over 5kHz', sqrt(CPS_xx(end)))

%% Noise estimation in translation
V_mean = 1.5; % [v]

G = 1000e3; % Gain in [nm/-]

Dx = G*(ur + ul - dr - dl)./(4*V_mean + ur + ul + dr + dl); % [nm]

figure;
plot(t, Dx)
xlabel('Time [s]');
ylabel('Displacement [nm]');

[Pxx, f] = pwelch(Dx, win, [], [], 1/Ts);

figure;
plot(f, sqrt(Pxx))
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [nm/$\sqrt{Hz}$]');
xlim([1, 5e3]);

CPS_xx = cumtrapz(f, Pxx);

figure;
plot(f, sqrt(CPS_xx))
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Cumulative ASD [$\mu$m]');
xlim([1, 5e3]);

sprintf('Electronic Noise for Rotations = %.1f [nm] over 5kHz', sqrt(CPS_xx(end)))

