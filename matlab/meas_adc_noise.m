%% Measurement of the ADC Noise
clear; clc; close all;
colors = colororder;

%% Measurement with 50 Ohm terminators at the ADC inputs
load('mat/meas_adc_noise_io332.mat');

%% Compute PSD of measured voltage
win = hanning(ceil(1*Fs)); % Hannning Windows

[Pur, f] = pwelch(detrend(ur, 0), win, [], [], 1/Ts);
[Pul, ~] = pwelch(detrend(ul, 0), win, [], [], 1/Ts);
[Pdr, ~] = pwelch(detrend(dr, 0), win, [], [], 1/Ts);
[Pdl, ~] = pwelch(detrend(dl, 0), win, [], [], 1/Ts);

%% Plot ASD of ADC
figure;
hold on;
plot(f, sqrt(Pur), 'DisplayName', 'ADC1')
plot(f, sqrt(Pul), 'DisplayName', 'ADC2')
plot(f, sqrt(Pdr), 'DisplayName', 'ADC3')
plot(f, sqrt(Pdl), 'DisplayName', 'ADC4')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [$V/\sqrt{Hz}$]');
xlim([1, 5e3]); ylim([1e-6, 1e-2]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 2);

exportFig('figs/adc_noise', 'height', 'normal', 'width', 'large')

%% Measurement with 50 Ohm terminators after 5m cable
load('mat/meas_adc_noise_5m_cable.mat');

%% Compute PSD of measured voltage
[Pur_5m, f] = pwelch(detrend(ur, 0), win, [], [], 1/Ts);
[Pul_5m, ~] = pwelch(detrend(ul, 0), win, [], [], 1/Ts);
[Pdr_5m, ~] = pwelch(detrend(dr, 0), win, [], [], 1/Ts);
[Pdl_5m, ~] = pwelch(detrend(dl, 0), win, [], [], 1/Ts);

%% Compare ASD of ADC noise with and with 5m cable
figure;
hold on;
plot(f, sqrt(Pur), 'color', [colors(1,:), 0.5], 'DisplayName', 'direct')
plot(f, sqrt(Pul), 'color', [colors(1,:), 0.5], 'HandleVisibility', 'off')
plot(f, sqrt(Pdr), 'color', [colors(1,:), 0.5], 'HandleVisibility', 'off')
plot(f, sqrt(Pdl), 'color', [colors(1,:), 0.5], 'HandleVisibility', 'off')
plot(f, sqrt(Pur_5m), 'color', [colors(2,:), 0.5], 'DisplayName', '5m cable')
plot(f, sqrt(Pul_5m), 'color', [colors(2,:), 0.5], 'HandleVisibility', 'off')
plot(f, sqrt(Pdr_5m), 'color', [colors(2,:), 0.5], 'HandleVisibility', 'off')
plot(f, sqrt(Pdl_5m), 'color', [colors(2,:), 0.5], 'HandleVisibility', 'off')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [$V/\sqrt{Hz}$]');
xlim([1, 5e3]); ylim([1e-6, 1e-2]);
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 1);

exportFig('figs/adc_noise_comp_5m', 'height', 'normal', 'width', 'large')