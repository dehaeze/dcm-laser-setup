%%
clear; clc; close all;
colors = colororder;

%% Piezo Stage is turned ON
load('mat/first-test.mat')

%%
win = hanning(ceil(20*Fs)); % Hannning Windows

%% 
[Pur_on, f] = pwelch(detrend(x, 0), win, [], [], 1/Ts);
[Pul_on, ~] = pwelch(detrend(y, 0), win, [], [], 1/Ts);
[Pdr_on, ~] = pwelch(detrend(dr, 1), win, [], [], 1/Ts);
[Pdl_on, ~] = pwelch(detrend(dl, 1), win, [], [], 1/Ts);

%%
figure;
hold on;
plot(f, sqrt(Pur_on), 'DisplayName', 'ur')
plot(f, sqrt(Pul_on), 'DisplayName', 'ul')
plot(f, 2e-6*ones(size(f)), 'k--', 'DisplayName', 'ADC Noise')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [$V/\sqrt{Hz}$]');
xlim([1, 5e3])

exportFig('figs/asd_photodiodes_xy_on', 'height', 'normal', 'width', 'large');

%% Piezo Stage is turned OFF
load('mat/meas_noise_xy_stage_off.mat')

[Pur_off, ~] = pwelch(detrend(ur, 0), win, [], [], 1/Ts);
[Pul_off, ~] = pwelch(detrend(ul, 0), win, [], [], 1/Ts);
[Pdr_off, ~] = pwelch(detrend(dr, 0), win, [], [], 1/Ts);
[Pdl_off, ~] = pwelch(detrend(dl, 0), win, [], [], 1/Ts);

%%
figure;
hold on;
plot(f, sqrt(Pur_on), 'color', colors(1, :), 'DisplayName', 'xy on')
plot(f, sqrt(Pul_on), 'color', colors(1, :), 'HandleVisibility', 'off')
plot(f, sqrt(Pdr_on), 'color', colors(1, :), 'HandleVisibility', 'off')
plot(f, sqrt(Pdl_on), 'color', colors(1, :), 'HandleVisibility', 'off')
plot(f, sqrt(Pul_off), 'color', colors(2, :), 'DisplayName', 'xy off')
plot(f, sqrt(Pdr_off), 'color', colors(2, :), 'HandleVisibility', 'off')
plot(f, sqrt(Pdl_off), 'color', colors(2, :), 'HandleVisibility', 'off')
plot(f, 2e-6*ones(size(f)), 'k--', 'DisplayName', 'ADC Noise')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('Ampl. Spectral Density [$V/\sqrt{Hz}$]');
xlim([1, 5e3])
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 1);

exportFig('figs/asd_photodiodes_xy_on_off', 'height', 'normal', 'width', 'large');

