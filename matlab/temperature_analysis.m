%%
clear;
load('mat/test-night-no-protect.mat')

H = 1.2; % Height [m]
L = 0.32; % Depth [m]

figure;
hold on;
plot(1/3600*(1:10:10*length(temp(:,1))), 1e9*H*12e-6*L*detrend(((temp(:,1)+temp(:,3)) - (temp(:,2)+temp(:,4))) + ((temp(:,5)+temp(:,7)) - (temp(:,8)+temp(:,6))), 0), 'DisplayName', 'Est. from T')
plot(1/3600*t, 200*detrend(Ry, 0), 'DisplayName', '4QD')
hold off;
xlabel('Time [h]');
ylabel('Angle [nrad]')
legend('location', 'southeast')

exportFig('figs/comp_temp_4qd.pdf', 'width', 'normal', 'height', 'tall');

%%
load('mat/test-night-protection.mat');

figure;
hold on;
plot(1/3600*t, 1e9*H*12e-6*L*detrend(((temp(:,1)+temp(:,3)) - (temp(:,2)+temp(:,4))) + ((temp(:,5)+temp(:,7)) - (temp(:,8)+temp(:,6))), 0), 'DisplayName', 'Est. from T')
plot(1/3600*t, 187500*detrend(Ry, 0), 'DisplayName', '4QD')
hold off;
xlabel('Time [h]');
ylabel('Angle [nrad]')
legend('location', 'southeast')

%% Comp
a = load('mat/meas_temp_first_night.mat');
b = load('mat/test-night-protection.mat');

figure;
hold on;
plot(b.t, b.temp(:,1))
plot(a.t, detrend(a.temp(1:length(a.t),1)-a.temp(1:length(a.t),2),0))


H = 1.2; % Height [m]
L = 0.32; % Depth [m]

figure;
hold on;
plot(b.t, 1e9*H*12e-6*L*detrend(b.temp(:,1)+b.temp(:,3)-b.temp(:,2)-b.temp(:,4), 0), '-')
plot(a.t, 1e9*H*12e-6*L*detrend(a.temp(1:length(a.t),1)+a.temp(1:length(a.t),3)-a.temp(1:length(a.t),2)-a.temp(1:length(a.t),4), 0), '-')

