%% Get Data from Speedgoat
tg = slrt;

f = SimulinkRealTime.openFTP(tg);
mget(f, 'data.dat');
close(f);

%% Load the data on the Workspace
data_dat = SimulinkRealTime.utils.getFileScopeData('data.dat');

data = struct();
data.Rx = data_dat.data(:,1); % [nrad]
data.Ry = data_dat.data(:,2); % [nrad]
data.Dx = data_dat.data(:,3); % [nm]
data.Dy = data_dat.data(:,4); % [nm]
data.Rz = data_dat.data(:,5); % [nrad]
data.t = data_dat.data(:,end);

data.Ts = (data.t(end) - data.t(1))/(length(data.t)-1);
data.Fs = 1/data.Ts;

save('mat/metrology_drifts_weekend.mat', '-struct', 'data')

%%
clear all;
load('metrology_drifts_weekend.mat');

%%
i = t > 60;

%%
figure;
hold on;
plot(t(i)/3600, detrend(Rx(i), 1), 'DisplayName', 'Rx');
plot(t(i)/3600, detrend(Ry(i), 1), 'DisplayName', 'Ry');
hold off;
xlabel('Time [h]');
ylabel('Angle [nrad]');
xlim([0,60]);
legend;

%%
figure;
hold on;
plot(t(i)/3600, 1e-3*detrend(Dx(i), 0), 'DisplayName', 'Dx');
plot(t(i)/3600, 1e-3*detrend(Dy(i), 0), 'DisplayName', 'Dy');
xlabel('Time [h]');
ylabel('Translation [$\mu$m]');
xlim([0, 60]);
legend;

%% Comparison between Attocube and 4QD
figure;
hold on;
plot(t(i)/3600, 1e-3*detrend(Rx(i), 0), 'DisplayName', 'Rx');
plot(t(i)/3600, 1e-3*detrend(-Rz(i), 0), 'DisplayName', 'Attocube');
xlabel('Time [h]');
ylabel('Translation [$\mu$rad]');
xlim([0, 60]);
legend;

%%
figure;
hold on;
plot(t(i)/3600, 1e-3*detrend(Dx(i), 0)/1.5, 'DisplayName', 'Dx');
plot(t(i)/3600, 1e-3*detrend(Rz(i), 0), 'DisplayName', 'Attocube');
xlabel('Time [h]');
ylabel('Rotation [$\mu$rad]');
xlim([0, 60]);
legend;

%%
figure;
hold on;
plot(t(i)/3600, 1e-3*detrend(Dx(i), 0)/1.5, 'DisplayName', 'Dx');
plot(t(i)/3600, 1e-3*detrend(Rz(i), 0), 'DisplayName', 'Attocube');
xlabel('Time [h]');
ylabel('Rotation [$\mu$rad]');
xlim([0, 60]);
legend;
