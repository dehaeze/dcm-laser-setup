function [data_struct] = extractData(data, names, i_start)

data_struct = struct();

%% Populate Struct
for i = 1:length(names)
      data_struct.(names{i}) = data.data(i_start:end, ismember(data.signalNames, names{i}));
end

data_struct.t = data.data(i_start:end, ismember(data.signalNames, 'Time'));

end

