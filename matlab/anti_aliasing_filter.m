s = ;
w0 = ; % Cut-off frequency [rad/s]
G = ;

G_aa = c2d(1/(1 + zpk('s')/(0.5*2*pi)), 1e-4, 'Tustin')

opts = bodeoptions('cstprefs');
opts.PhaseVisible = 'off';
opts.FreqUnits = 'Hz';

figure;
bodeplot(G_aa,opts)